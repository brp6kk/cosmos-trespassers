# Cosmos Trespassers
Uncommon Hacks 2021 Project - Eric Chang

## Description

* Shameless Space Invaders knockoff
* Built using the [Pixi.js](https://www.pixijs.com/) library and not much else
* Stole the pixel art from [here](https://www.andoverpatio.co.uk/21/space-invaders/)

## Ready to play?

### Setup

* Download the repository

![](static/img/readme/step01-01.png)
![](static/img/readme/step01-02.png)

* Launch a webserver in the root directory
  * Recommendation: [http-server](https://www.npmjs.com/package/http-server)

![](static/img/readme/step02.png)

* Navigate to any url on which the server is available

![](static/img/readme/step03-01.png)
![](static/img/readme/step03-02.png)

### Controls

* Left & right arrow keys - move left & right
* Space - shoot a bullet
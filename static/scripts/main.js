/**********
 * 
 */ 


/********** VARIABLES **********/

const STAGE_SIZE = 480;

let app = new PIXI.Application({
    width: STAGE_SIZE, 
    height: STAGE_SIZE,
    antialias: true,
    transparent: true,
    resolution: 1
});

let loader = PIXI.Loader.shared;

let style = new PIXI.TextStyle({
    fontFamily: "Courier New",
    fontSize: 24,
    fill: "white",
});

let state = undefined;

const SCALE = 0.5;
const USER_VELOCITY = 5;
const BULLET_VELOCITY = 4;
const SCORE_MESSAGE_START = "SCORE: ";
const LIVES_MESSAGE_START = "LIVES: ";

/********** END VARIABLES **********/


/********** SETUP **********/

// Add app to page.
document.body.appendChild(app.view);

// Load in images.
// Calls setup() once complete.
loader.add("alien1", "static/img/alien1.png").
       add("alien2", "static/img/alien2.png").
       add("alien3", "static/img/alien3.png").
       add("alien4", "static/img/alien4.png").
       add("fullblock", "static/img/block1.png").
       add("brokenBlock1", "static/img/block2.png").
       add("brokenBlock2", "static/img/block3.png").
       add("brokenBlock3", "static/img/block4.png").
       add("player", "static/img/player.png").
       load(setup);

// Main setup function.
function setup() {
    setupGame();
    
    app.ticker.add(delta => gameLoop(delta));
}

// Set up the actual game itself.
function setupGame() {
    state = startState();
    setupPlayerSprite();
    setupKeyboardEvents();
    setupNewStage();
    setupText();
}

// Set up a stage within the game.
function setupNewStage() {
    // Remove any leftover bullets that may still exist.
    if (state["enemyBullet"]) {
        app.stage.removeChild(state["enemyBullet"]);
        state["enemyBullet"] = undefined;
    }
    
    // Remove a leftover rogue enemy that may still exist.
    if (state["enemyRogue"]) {
        app.stage.removeChild(state["enemyRogue"]);
        state["enemyRogue"] = undefined;
    }
    
    setupArmySprite();
    setupBlockadesSprite();
}

// Set up alien army sprites.
function setupArmySprite() {
    let topSpace = 100;
    
    // Originally did a PIXI.Container,
    // but that caused some issues with position
    // after clearing out leftmost columns of aliens.
    // Since some brief Googling did not reveal
    // a good solution, aliens are painted directly
    // onto the stage and stored in an array.
    state["enemyArmy"] = [];
    
    for (let i = 0; i < 5; i++) {
        let texture = chooseAlienTexture(i);
        for (let j = 0; j < 11; j++) {
            let sprite = new PIXI.Sprite(texture);
            sprite.scale.set(SCALE, SCALE);
            sprite.x = j*(50*SCALE + 5);
            sprite.y = topSpace + i*(50*SCALE + 5);
            
            state["enemyArmy"].push(sprite);
            app.stage.addChild(sprite);
        }
    }
}

// Set up blockade sprites.
function setupBlockadesSprite() {
    let leftSpace = 54; // pixels between blockades
    let topSpace = 375;
    let texture = loader.resources.fullblock.texture;
    state["blockades"] = new PIXI.Container();
    state["blockades"].y = topSpace;
    
    // 4 blockades total
    for (let i = 0; i < 4; i++) {
        // 4 blocks per column
        for (let j = 0; j < 4; j++) {
            // 2 full rows per column
            for (let k = 0; k < 2; k++) {
                let sprite = new PIXI.Sprite(texture);
                sprite.scale.set(SCALE, SCALE);
                sprite.x = leftSpace + (2*leftSpace*i) + j*(25*SCALE);
                sprite.y = k*(25*SCALE);
                state["blockades"].addChild(sprite);
            }
        }
        
        // Last row is not full, coded separately
        let spriteY = 2*(25*SCALE);
        let sprite = new PIXI.Sprite(texture);
        sprite.scale.set(SCALE, SCALE);
        sprite.x = leftSpace + (2*leftSpace*i);
        sprite.y = spriteY;
        state["blockades"].addChild(sprite);
        
        sprite = new PIXI.Sprite(texture);
        sprite.scale.set(SCALE, SCALE);
        sprite.x = leftSpace + (2*leftSpace*i) + 3*(25*SCALE);
        sprite.y = spriteY;
        state["blockades"].addChild(sprite);
    }
    
    app.stage.addChild(state["blockades"]);
}

// Set up player sprite.
function setupPlayerSprite() {
    let topSpace = 425
    let texture = loader.resources.player.texture;
    
    let sprite = new PIXI.Sprite(texture);
    sprite.scale.set(SCALE, SCALE);
    sprite.x = STAGE_SIZE / 2 - sprite.width / 2;
    sprite.y = topSpace;
    state["player"] = sprite;
    
    app.stage.addChild(state["player"])
}

// Defines how to respond to keyboard events.
function setupKeyboardEvents() {
    let keyObject;
    
    // Move player to the left.
    keyObject = keyboard("ArrowLeft");
    keyObject.press = function() {
        state["player"].vx = -USER_VELOCITY;
    }
    keyObject.release = function() {
        if (state["player"].vx == -USER_VELOCITY) {
            state["player"].vx = 0;
        }
    }
    state["keyObjects"].push(keyObject);
    
    // Move player to the right.
    keyObject = keyboard("ArrowRight");
    keyObject.press = function() {
        state["player"].vx = USER_VELOCITY
    }
    keyObject.release = function() {
        if (state["player"].vx == USER_VELOCITY) {
            state["player"].vx = 0;
        }
    }
    state["keyObjects"].push(keyObject);
    
    // Player shoots a bullet.
    keyObject = keyboard(" ");
    keyObject.press = function() {
        // Only one bullet exists at a time.
        if (!state["playerBullet"]) {
            let bulletX = state["player"].x + state["player"].width/2;
            let bulletY = state["player"].y;
            
            let bullet = createBulletSprite(bulletX, bulletY);
            state["playerBullet"] = bullet;
            app.stage.addChild(state["playerBullet"]);
        }
    }
    state["keyObjects"].push(keyObject);
}

// Writes the player's score
// and number of lives left on the screen.
function setupText() {
    let text, message;
    
    message = SCORE_MESSAGE_START + state["score"];
    text = new PIXI.Text(message, style);
    state["scoreMessage"] = text;
    app.stage.addChild(state["scoreMessage"]);
    
    message = LIVES_MESSAGE_START + state["lives"];
    text = new PIXI.Text(message, style);
    text.y = STAGE_SIZE - text.height;
    state["livesMessage"] = text;
    app.stage.addChild(state["livesMessage"]);
}

/********** END SETUP **********/


/********** GAME LOGIC **********/

// Main game loop function.
// Executes 60 times per second.
function gameLoop(delta) {
    // State only exists during an active game.
    if (state) {
        movePlayer();
        movePlayerBullet();
        moveRogue();
        moveArmy();
        
        // Army might have collided with player,
        // causing a gameover.
        if (state) {
            moveArmyBullet();
        }
    }
}

// Move player based on current velocity.
function movePlayer() {
    // Update position if the user 
    // hasn't moved off the screen.
    let newPosition = state["player"].x + state["player"].vx;
    if (newPosition >=0 && newPosition <= STAGE_SIZE - state["player"].width) {
        state["player"].x = newPosition;
    }
}

// Move the player's bullet,
// if it exists.
function movePlayerBullet() {
    if (state["playerBullet"]) {
        let newPosition = state["playerBullet"].y - BULLET_VELOCITY;
        if (newPosition < 0) {
            // Bullet moved off screen
            app.stage.removeChild(state["playerBullet"]);
            state["playerBullet"] = undefined;
        } else {
            state["playerBullet"].y = newPosition; 
            
            checkPlayerBulletCollideBlockade();
            checkPlayerBulletCollideAlien();
        }
    }
}

// Check if the player's bullet has collided 
// with a blockade.
function checkPlayerBulletCollideBlockade() {
    let bullet = state["playerBullet"];
    let result = checkBulletCollideBlockade(bullet);
    
    // either bullet didn't exist to begin with,
    // or it was destroyed along with a block.
    if (!result) {
        state["playerBullet"] = undefined;
    }
}

// Check if player's bullet has collided with any alien,
// killing the alien if so.
function checkPlayerBulletCollideAlien() {
    if (state["playerBullet"]) {
        let army = state["enemyArmy"];
        for (let i = 0; i < army.length; i++) {
            if (haveCollided(state["playerBullet"], army[i])) {
                let deadAlien = army[i];
            
                // Update score.
                let addition = getEnemyScore(deadAlien.texture);
                state["score"] += addition;
                let message = SCORE_MESSAGE_START + state["score"];
                state["scoreMessage"].text = message;
            
                // Remove dead alien & bullet.
                army.splice(i, 1);
                app.stage.removeChild(deadAlien);
                app.stage.removeChild(state["playerBullet"]);
                state["playerBullet"] = undefined;
            
                // Killed all the aliens.
                if (army.length == 0) {
                    setupNewStage();
                } 
            
                // No need to check rest of aliens -
                // only one can be killed at a time.
                break;
            }
        }
        
        if (state["enemyRogue"] && state["playerBullet"] && 
                haveCollided(state["enemyRogue"], state["playerBullet"])) {
            // Update score.
            let addition = getEnemyScore(state["enemyRogue"].texture);
            state["score"] += addition;
            let message = SCORE_MESSAGE_START + state["score"];
            state["scoreMessage"].text = message;
            
            app.stage.removeChild(state["enemyRogue"]);
            state["enemyRogue"] = undefined;
            app.stage.removeChild(state["playerBullet"]);
            state["playerBullet"] = undefined;
        }
    }
}

// Move the army of enemies.
function moveArmy() {
    let changeDirection = false;
    
    for (let i = 0; i < state["enemyArmy"].length; i++) {
        let newPosition = state["enemyArmy"][i].x + state["enemyVelocity"]
        state["enemyArmy"][i].x = newPosition;
        
        // Change direction if any of the aliens reach
        // the edge of the board.
        changeDirection = changeDirection ||
            state["enemyArmy"][i].x < 0 ||
            state["enemyArmy"][i].x + state["enemyArmy"][i].width > STAGE_SIZE;
    }
    
    if (changeDirection) {
        state["enemyVelocity"] = -state["enemyVelocity"];
        // Also have to move down.
        for (let i = 0; i < state["enemyArmy"].length; i++) {
            state["enemyArmy"][i].y += USER_VELOCITY;
        }
    }
    
    checkArmyCollidePlayer();
}

// Check if any aliens have collided with the player.
// Immediate game over if so.
function checkArmyCollidePlayer() {
    for (let i = 0; i < state["enemyArmy"].length; i++) {
        if (haveCollided(state["enemyArmy"][i], state["player"])) {
            gameOver();
            break;
        }
    }
}

// Move the bullet shot by an enemy.
function moveArmyBullet() {
    // Only one enemy shoots a bullet at a time.
    if (!state["enemyBullet"]) {
        let attacker = getRandomEnemy();
        let bulletX = attacker.x + attacker.width / 2;
        let bulletY = attacker.y + attacker.height;
        
        let bullet = createBulletSprite(bulletX, bulletY);
        state["enemyBullet"] = bullet;
        app.stage.addChild(state["enemyBullet"]);
    } else {
        let newPosition = state["enemyBullet"].y + BULLET_VELOCITY;
        if (newPosition > STAGE_SIZE) {
            // Bullet moved off screen
            app.stage.removeChild(state["enemyBullet"]);
            state["enemyBullet"] = undefined;
        } else {
            state["enemyBullet"].y = newPosition;
            checkAlienBulletCollideBlockade();
            checkAlienBulletCollidePlayer();
        }
    }
}

// Check if the alien's bullet has collided
// with a blockade.
function checkAlienBulletCollideBlockade() {
    let bullet = state["enemyBullet"];
    let result = checkBulletCollideBlockade(bullet);
    
    // either bullet didn't exist to begin with,
    // or it was destroyed along with a block.
    if (!result) {
        state["enemyBullet"] = undefined;
    }
}

// Check if alien's bullet has collided with the player,
// removing a life if so.
function checkAlienBulletCollidePlayer() {
    if (state["enemyBullet"]) {
        if (haveCollided(state["enemyBullet"], state["player"])) {
            app.stage.removeChild(state["enemyBullet"]);
            state["enemyBullet"] = undefined;
        
            // Bullet hit player - remove a life.
            state["lives"] -= 1;
            if (state["lives"] <= 0) {
                gameOver();
            } else {
                let message = LIVES_MESSAGE_START + state["lives"];
                state["livesMessage"].text = message;
            }
        }
    }
}

// Move the rogue alien enemy.
function moveRogue() {
    if (!state["enemyRogue"]) {
        generateRogue();
    } else {
        state["enemyRogue"].x -= 2;
        if (state["enemyRogue"].x < -state["enemyRogue"].width) {
            app.stage.removeChild(state["enemyRogue"]);
            state["enemyRogue"] = undefined;
        }
    }
}

/********** END GAME LOGIC **********/

/********** ENDGAME **********/

// Cleans up stage after game over.
function gameOver() {
    // Clear stage.
    while(app.stage.children[0]) {
        app.stage.removeChild(app.stage.children[0]);
    }
    
    // Add game over text to stage.
    let score = state["score"];
    let messages = ["FINAL SCORE: " + score,
                    "Press [space] to play again"];
    for (let i = 0; i < messages.length; i++) {
        let text = new PIXI.Text(messages[i], style);
        text.x = STAGE_SIZE / 2 - text.width / 2;
        text.y = i*50 + STAGE_SIZE / 2 - 25;
        app.stage.addChild(text);
    }
    
    // Clear keyboard events.
    let keyObjects = state["keyObjects"];
    for (let i = 0; i < keyObjects.length; i++) {
        keyObjects[i].unsubscribe();
    }
    
    // Add keyboard event to restart game.
    let keyObject = keyboard(" ");
    keyObject.press = function() {
        this.unsubscribe();
        while(app.stage.children[0]) {
            app.stage.removeChild(app.stage.children[0]);
        }
        setupGame();
    }
    
    // Prevents game loop from trying to access stuff.
    state = undefined;
}

/********** END ENDGAME **********/

/********** HELPERS **********/

// Chooses the appropriate texture of an alien
// based on the row it occupies within the army.
function chooseAlienTexture(num) {
    let retVal;
    
    switch(num) {
        case 0:
            retVal = loader.resources.alien3.texture;
            break;
        case 1:
        case 2:
            retVal = loader.resources.alien2.texture;
            break;
        default: // row 3 or 4
            retVal = loader.resources.alien1.texture;
            break;
    }
    
    return retVal;
}

// Used to create representations of keyboard keys.
// Heavily based on:
// https://github.com/kittykatattack/learningPixi/blob/master/examples/12_keyboardMovement.html
function keyboard(value) {
    let key = {};
    key["value"] = value;
    key.isDown = false;
    key.isUp = true;
    key.press = undefined;
    key.release = undefined;
    
    key.downHandler = function(e) {
        if (event.key === key.value) {
            if (key.isUp && key.press) key.press();
            key.isDown = true;
            key.isUp = false;
            e.preventDefault();
        }
    };
    
    key.upHandler = function(e) {
        if (event.key === key.value) {
            if (key.isDown && key.release) key.release();
            key.isDown = false;
            key.isUp = true;
            e.preventDefault();
        }
    };
    
    const downListener = key.downHandler.bind(key);
    const upListener = key.upHandler.bind(key);
    
    window.addEventListener("keydown", downListener, false);
    window.addEventListener("keyup", upListener, false);
    
    key.unsubscribe = () => {
        window.removeEventListener("keydown", downListener);
        window.removeEventListener("keyup", upListener);
    };
    
    return key;
}

// Helper to create a bullet sprite.
// x, y => x and y position.
function createBulletSprite(x, y) {
    let bullet = new PIXI.Graphics();
    bullet.beginFill(0xffffff);
    bullet.drawRect(0, 0, 2, 5);
    bullet.endFill();
    bullet.x = x;
    bullet.y = y;
    
    return bullet;
}

// Helper to get a random enemy
// from the current container of enemies.
function getRandomEnemy() {
    let armySize = state["enemyArmy"].length;
    let enemyI = Math.floor(Math.random() * armySize);
    let enemy = state["enemyArmy"][enemyI];
    
    return enemy;
}

// Helper that creates a new state 
// for the beginning of a game.
function startState() {
    return {
        // Sprite/Container variables
        "enemyArmy": undefined,
        "enemyBullet": undefined,
        "enemyRogue": undefined,
        "player": undefined,
        "playerBullet": undefined,
        "blockades": undefined,
        "scoreMesage": undefined,
        "livesMessage": undefined,
    
        // Misc. variables
        "score": 0,
        "lives": 3,
        "enemyVelocity": 1,
        "keyObjects": [],
    }
}

// Determines if two elements overlap.
// Heavily based on hitTestRectangle from:
// https://github.com/kittykatattack/learningPixi/blob/master/examples/16_collisionDetection.html
function haveCollided(r1, r2) {
    // Find the center points of each sprite
    r1.centerX = r1.getGlobalPosition().x + r1.width / 2; 
    r1.centerY = r1.getGlobalPosition().y + r1.height / 2; 
    r2.centerX = r2.getGlobalPosition().x + r2.width / 2; 
    r2.centerY = r2.getGlobalPosition().y + r2.height / 2; 

    // Find the half-widths and half-heights of each sprite
    r1.halfWidth = r1.width / 2;
    r1.halfHeight = r1.height / 2;
    r2.halfWidth = r2.width / 2;
    r2.halfHeight = r2.height / 2;

    // Calculate the distance vector between the sprites
    let vx = r1.centerX - r2.centerX;
    let vy = r1.centerY - r2.centerY;

    // Figure out the combined half-widths and half-heights
    let combinedHalfWidths = r1.halfWidth + r2.halfWidth;
    let combinedHalfHeights = r1.halfHeight + r2.halfHeight;

    // Check for a collision on the x axis
    // and on the y axis.
    let hit = ((Math.abs(vx) < combinedHalfWidths) &&
               (Math.abs(vy) < combinedHalfHeights));

    return hit;
}

// Gets score increase for a particular enemy.
// texture => Texture object from an enemy sprite.
function getEnemyScore(texture) {
    let retVal;
    switch(texture) {
        case loader.resources.alien1.texture:
            retVal = 10;
            break;
        case loader.resources.alien2.texture:
            retVal = 20;
            break;
        case loader.resources.alien3.texture:
            retVal = 30;
            break;
        default: // Rogue enemy.
            retVal = 100;
            break;
    }
    
    return retVal;
}

// Returns the appropriate texture for a block
// after a block with the passed texture is
// hit by a bullet.
// Returns undefined if the block is now
// completely broken (and should be destroyed).
function getNextBlockTexture(texture) {
    let retVal;
    switch(texture) {
        case loader.resources.fullblock.texture:
            retVal = loader.resources.brokenBlock1.texture;
            break;
        case loader.resources.brokenBlock1.texture:
            retVal = loader.resources.brokenBlock2.texture;
            break;
        case loader.resources.brokenBlock2.texture:
            retVal = loader.resources.brokenBlock3.texture;
            break;
        default: // block completely broken
            retVal = undefined;
            break;
    }
    
    return retVal;
}

// Check if passed bullet has collided with a blockade,
// destroying the block if so.
// Returns undefined if the bullet is destroyed,
// returns the original bullet if no blockades are hit.
function checkBulletCollideBlockade(bullet) {
    if (!bullet) {
        return undefined;
    }
    
    let blockades = state["blockades"];

    for (let i = 0; i < blockades.children.length; i++) {
        if (haveCollided(bullet, blockades.children[i])) {
            let destroyedBlock = blockades.children[i];
            let newTexture = getNextBlockTexture(destroyedBlock.texture);
            
            if (newTexture) {
                blockades.children[i].texture = newTexture;
            } else {
                // No new texture = block completely destroyed.
                blockades.removeChild(destroyedBlock);
            }
            
            app.stage.removeChild(bullet);
            return undefined;
        }
    }
    
    // no collisions
    return bullet;
}

// Potentially generate a rogue alien enemy.
function generateRogue() {
    // If rogue alien doesn't exist,
    // there's a 1/512 chance that one will
    // be created during this frame.
    let createRogue = Math.floor(Math.random() * 512) < 1;
    
    if (createRogue) {
        let texture = loader.resources.alien4.texture;
        let sprite = new PIXI.Sprite(texture);
        sprite.scale.set(SCALE, SCALE);
        sprite.x = STAGE_SIZE;
        sprite.y = 50;
        state["enemyRogue"] = sprite;
        
        app.stage.addChild(state["enemyRogue"]);
    }
}

/********** END HELPERS **********/